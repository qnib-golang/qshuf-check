package qyaml

import (
	"fmt"
	"io/ioutil"
	"log"

	"gitlab.com/gnib-golang/qshuf-check/pkg/files"

	"github.com/urfave/cli/v2"
	yaml "gopkg.in/yaml.v2"
)

type Requirements struct {
	Software []Software `yaml:"software"`
}

type Software struct {
	Name string  `yaml:"name"`
	Min  Version `yaml:"min"`
	Max  Version `yaml:"max"`
}

type Version struct {
	Major int `yaml:"major"`
	Minor int `yaml:"minor"`
	Patch int `yaml:"patch"`
}

func ParseRequirements(c *cli.Context) (r Requirements, err error) {
	fname := c.String("requirements")
	var yamlFile []byte
	if fname != "" {
		yamlFile, err = ioutil.ReadFile(fname)
	} else {

		ws := c.String("workshop")
		yamlFile, err = files.GetFile(ws)
		if err != nil {
			log.Fatal(err)
		}
	}

	if err != nil {
		fmt.Printf("Error reading YAML file: %s\n", err)
		return
	}
	err = yaml.Unmarshal(yamlFile, &r)
	if err != nil {
		fmt.Printf("Error parsing YAML file: %s\n", err)
	}
	return
}

func GetWorkshops() []string {
	return []string{"container-runtimes-101"}
}
