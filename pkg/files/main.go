package files

import (
	"embed"
	"fmt"
	"strings"
)

// Also tried "files"
//go:embed *.yaml
var embedFS embed.FS

func GetFile(ws string) ([]byte, error) {
	return embedFS.ReadFile(fmt.Sprintf("%s.yaml", ws))
}

func ListWorkshops() (res []string, err error) {
	path := "."
	res = []string{}
	entries, err := embedFS.ReadDir(path)
	if err != nil {
		return nil, err
	}
	for _, entry := range entries {
		if entry.IsDir() {
			continue
		}
		fname := entry.Name()
		fname = strings.TrimSuffix(fname, ".yaml")
		res = append(res, fname)
	}

	return
}
