package requirements

import "fmt"

var (
	sysTool      = "system-tool"
	cntTool      = "container tool"
	cntEngine    = "container engine"
	cntHpcEngine = "HPC container engine"
	cntRuntime   = "container runtime"
	hpcSched     = "HPC scheduler"
	pkgs         = map[string]Software{
		"jq":      NewSoftware("jq", sysTool, "--version", `jq-(?P<Major>\d+)\.(?P<Minor>\d+)`),
		"docker":  NewSoftware("docker", cntEngine, "--version", `Docker version (?P<Major>\d+)\.(?P<Minor>\d+)\.(?P<Patch>\d+)`),
		"wildq":   NewSoftware("wildq", sysTool, "--version", `wildq, version (?P<Major>\d+)\.(?P<Minor>\d+)\.(?P<Patch>\d+)`),
		"chroot":  NewBinarySoftware("chroot", sysTool),
		"skopeo":  NewSoftware("skopeo", cntTool, "--version", `skopeo version (?P<Major>\d+)\.(?P<Minor>\d+)\.(?P<Patch>\d+)`),
		"runc":    NewSoftware("runc", cntRuntime, "--version", `runc version (?P<Major>\d+)\.(?P<Minor>\d+)\.(?P<Patch>\d+)`),
		"crun":    NewSoftware("crun", cntRuntime, "--version", `crun version (?P<Major>\d+)\.(?P<Minor>\d+)\.(?P<Patch>\d+)`),
		"sarus":   NewSoftware("sarus", cntHpcEngine, "--version", `(?P<Major>\d+)\.(?P<Minor>\d+)\.(?P<Patch>\d+)`),
		"enroot":  NewSoftware("enroot", cntHpcEngine, "version", `(?P<Major>\d+)\.(?P<Minor>\d+)\.(?P<Patch>\d+)`),
		"podman":  NewSoftware("podman", cntEngine, "--version", `podman version (?P<Major>\d+)\.(?P<Minor>\d+)\.(?P<Patch>\d+)`),
		"slurm":   NewSoftware("sinfo", hpcSched, "-V", `slurm (?P<Major>\d+)\.(?P<Minor>\d+)\.(?P<Patch>\d+)`),
		"buildah": NewSoftware("buildah", cntTool, "--version", `buildah version (?P<Major>\d+)\.(?P<Minor>\d+)\.(?P<Patch>\d+)`),
	}
)

type Packages map[string]Software

func GetPackage(name string) (s Software, err error) {
	s, ok := pkgs[name]
	if !ok {
		return s, fmt.Errorf("No package named %s", name)
	}
	return s, nil
}
