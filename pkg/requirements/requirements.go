package requirements

import (
	"fmt"
	"log"
	"os"
	"strings"

	qyaml "gitlab.com/gnib-golang/qshuf-check/pkg/yaml"
)

type Requirements struct {
	sw   map[string]Software
	frmt string
}

func NewRequirements(y qyaml.Requirements) (req Requirements, err error) {
	req = Requirements{
		sw:   make(map[string]Software),
		frmt: "| %-5s | %-10s | %-20s | %-50s | %-10s | %-10s | %-10s |\n",
	}
	for _, s := range y.Software {
		sw, err := GetPackage(s.Name)
		if err != nil {
			log.Println(err.Error())
			continue
		}
		sw.SetExpectedVersion(s)
		req.sw[s.Name] = sw
	}
	return
}

func (r Requirements) Eval() (err error) {
	r.PrintHeader()
	for name, sw := range r.sw {
		swr := NewSwResult(r.frmt, name, sw.Type)
		sw.Eval(&swr)
		r.sw[name] = sw
	}
	nok := []string{}
	for name, sw := range r.sw {
		if !sw.Ok {
			nok = append(nok, name)
		}
	}
	if len(nok) > 0 {
		log.Printf("Please check the following software: \n - %s", strings.Join(nok, "\n - "))
		os.Exit(1)
	}

	return
}

func (r *Requirements) PrintHeader() {
	fmt.Printf(r.frmt, "Status", "Name", "Type", "Path", "Version", "Min", "Max")
}
