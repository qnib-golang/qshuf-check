package requirements

import (
	"fmt"
	"io"
)

var (
	green = string([]byte{27, 91, 57, 50, 109})
	red   = string([]byte{27, 91, 51, 49, 109})
	reset = string([]byte{27, 91, 48, 109})
)

type SwResult struct {
	writer          io.Writer
	frmt            string
	Ok              bool
	Name            string
	Type            string
	Path            string
	DetectedVersion string
	ExpVersionMin   string
	ExpVersionMax   string
}

func NewSwResult(frmt string, n string, t string) SwResult {
	return SwResult{
		frmt: frmt,
		Name: n,
		Type: t,
		Ok:   true,
	}
}

func (s SwResult) PrintRow() {
	status := "OK"
	if s.Ok {
		status = fmt.Sprintf("%s%-6s%s", green, status, reset)
	} else {
		status = fmt.Sprintf("%s%-6s%s", red, "ERROR", reset)
	}
	fmt.Printf(s.frmt, status, s.Name, s.Type, s.Path, s.DetectedVersion, s.ExpVersionMin, s.ExpVersionMax)
}
