package requirements

import (
	"fmt"
	"log"
	"os/exec"
	"regexp"
	"strconv"
	"strings"

	qyaml "gitlab.com/gnib-golang/qshuf-check/pkg/yaml"
)

type Software struct {
	Binary          string
	Type            string
	Ok              bool
	Arg             []string
	RegEx           []*regexp.Regexp
	DetectedVersion Version
	ExpectedVersion Versions
}

func NewSoftware(b, t, a string, re ...string) Software {
	s := NewBinarySoftware(b, t)
	s.Type = t
	s.Arg = strings.Split(a, " ")
	s.DetectedVersion = Version{-1, -1, -1}
	for _, r := range re {
		s.RegEx = append(s.RegEx, regexp.MustCompile(r))
	}
	return s
}

func NewBinarySoftware(b, t string) Software {
	s := Software{
		Binary: b,
		Type:   t,
	}
	return s
}

func (s *Software) SetExpectedVersion(qsw qyaml.Software) {
	s.ExpectedVersion = Versions{}
	s.ExpectedVersion.Min.Major = qsw.Min.Major
	s.ExpectedVersion.Min.Minor = qsw.Min.Minor
	s.ExpectedVersion.Min.Patch = qsw.Min.Patch
	s.ExpectedVersion.Max.Major = qsw.Max.Major
	s.ExpectedVersion.Max.Minor = qsw.Max.Minor
	s.ExpectedVersion.Max.Patch = qsw.Max.Patch
}

func (s *Software) Eval(swr *SwResult) (err error) {
	swr.Ok = true
	minVer := []string{}
	if s.ExpectedVersion.Min.Major != -1 {
		minVer = append(minVer, fmt.Sprintf("%d", s.ExpectedVersion.Min.Major))
	}
	if s.ExpectedVersion.Min.Minor != -1 {
		minVer = append(minVer, fmt.Sprintf("%d", s.ExpectedVersion.Min.Minor))
	}
	if s.ExpectedVersion.Min.Patch != -1 {
		minVer = append(minVer, fmt.Sprintf("%d", s.ExpectedVersion.Min.Patch))
	}
	maxVer := []string{}
	if s.ExpectedVersion.Max.Major != 0 {
		maxVer = append(minVer, fmt.Sprintf("%d", s.ExpectedVersion.Max.Major))
	}
	if s.ExpectedVersion.Max.Minor != 0 {
		maxVer = append(minVer, fmt.Sprintf("%d", s.ExpectedVersion.Max.Minor))
	}
	if s.ExpectedVersion.Max.Patch != 0 {
		maxVer = append(minVer, fmt.Sprintf("%d", s.ExpectedVersion.Max.Patch))
	}
	swr.ExpVersionMin = strings.Join(minVer, ".")
	swr.ExpVersionMax = strings.Join(maxVer, ".")
	p, err := exec.Command("which", s.Binary).Output()
	if err != nil {
		swr.Ok = false
		s.Ok = false
		swr.PrintRow()
		return
	}

	swr.Path = strings.TrimSpace(string(p))
	if len(s.Arg) == 0 {
		s.Ok = swr.Ok
		swr.PrintRow()
		return
	}
	ver, err := exec.Command(s.Binary, s.Arg...).Output()
	if err != nil {
		swr.Ok = false
		swr.PrintRow()
		return
		//log.Fatalf("Error running command '%s' %v", s.Binary, err.Error())
	}
	for _, re := range s.RegEx {
		if m := re.FindStringSubmatch(string(ver)); m != nil {
			detectedVer := []string{}
			if len(m) > 1 {
				detectedVer = append(detectedVer, m[1])
				s.DetectedVersion.Major = s.parseInt(m[1])
			}
			if len(m) > 2 {
				detectedVer = append(detectedVer, m[2])
				s.DetectedVersion.Minor = s.parseInt(m[2])
			}
			if len(m) > 3 {
				detectedVer = append(detectedVer, m[3])
				s.DetectedVersion.Patch = s.parseInt(m[3])
			}
			swr.DetectedVersion = strings.Join(detectedVer, ".")

			switch len(m) {
			case 2:
				e := s.DetectedVersion.MinMajor(s.ExpectedVersion.Min.Major)
				if e != nil {
					swr.Ok = false
				}
				e = s.DetectedVersion.MaxMajor(s.ExpectedVersion.Max.Major)
				if e != nil {
					swr.Ok = false
				}
			case 3:
				e := s.DetectedVersion.MinMinor(s.ExpectedVersion.Min.Major, s.ExpectedVersion.Min.Minor)
				if e != nil {
					swr.Ok = false
				}
				e = s.DetectedVersion.MaxMinor(s.ExpectedVersion.Max.Major, s.ExpectedVersion.Max.Minor)
				if e != nil {
					swr.Ok = false
				}
			case 4:
				e := s.DetectedVersion.MinPatch(s.ExpectedVersion.Min.Major, s.ExpectedVersion.Min.Minor, s.ExpectedVersion.Min.Patch)
				if e != nil {
					swr.Ok = false
				}
				e = s.DetectedVersion.MaxPatch(s.ExpectedVersion.Max.Major, s.ExpectedVersion.Max.Minor, s.ExpectedVersion.Max.Patch)
				if e != nil {
					swr.Ok = false
				}
			}
			s.Ok = swr.Ok
			swr.PrintRow()
		}

	}
	return err
}

func (s Software) GetLine() (res []string) {
	res = []string{s.Binary}
	res = append(res, "")
	return
}

func (s Software) String() string {
	res := []string{fmt.Sprintf("%d", s.DetectedVersion.Major)}
	if s.DetectedVersion.Minor != -1 {
		res = append(res, fmt.Sprintf("%d", s.DetectedVersion.Minor))
	}
	if s.DetectedVersion.Patch != -1 {
		res = append(res, fmt.Sprintf("%d", s.DetectedVersion.Patch))
	}
	ver := strings.Join(res, ".")
	return fmt.Sprintf("%s: %s", s.Binary, ver)
}

func (s *Software) parseInt(in string) int {
	i, err := strconv.Atoi(in)
	if err != nil {
		log.Fatalf("Error parsing int '%v': %v", in, err)
	}
	return i
}
