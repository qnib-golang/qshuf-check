package requirements

import (
	"fmt"
	"strings"
)

type Versions struct {
	Min Version
	Max Version
}

type Version struct {
	Major int
	Minor int
	Patch int
}

func (v Version) Less(v2 Version) bool {
	if v.Major < v2.Major {
		return true
	}
	if v.Minor < v2.Minor {
		return true
	}
	if v.Patch < v2.Patch {
		return true
	}
	return false
}

func (v Version) String() (str string) {
	res := []string{fmt.Sprintf("%d", v.Major)}
	if v.Minor != -1 {
		res = append(res, fmt.Sprintf("%d", v.Minor))
	}
	if v.Patch != -1 {
		res = append(res, fmt.Sprintf("%d", v.Patch))
	}
	return strings.Join(res, ".")
}

func (v Version) Equal(v2 Version) bool {
	return v.Major == v2.Major && v.Minor == v2.Minor && v.Patch == v2.Patch
}

func (v Version) MinMajor(major int) (err error) {
	if v.Major < major {
		msg := fmt.Sprintf("%d < %d", v.Major, major)
		err = fmt.Errorf(msg)
	}
	return err
}

func (v Version) MaxMajor(major int) (err error) {
	if major == 0 {
		return nil
	}
	if v.Major > major {
		msg := fmt.Sprintf("%d > %d", v.Major, major)
		err = fmt.Errorf(msg)
	}
	return err
}

func (v Version) MinMinor(major, minor int) (err error) {
	maj := v.MinMajor(major)
	if maj != nil {
		return maj
	}
	if v.Major == major && v.Minor < minor {
		msg := fmt.Sprintf("%d.%d < %d.%d", v.Major, v.Minor, major, minor)
		err = fmt.Errorf(msg)
	}
	return err
}

func (v Version) MaxMinor(major, minor int) (err error) {
	maj := v.MaxMajor(major)
	if maj != nil {
		return maj
	}
	if v.Major == major && minor > 0 && v.Minor > minor {
		msg := fmt.Sprintf("%d.%d > %d.%d", v.Major, v.Minor, major, minor)
		err = fmt.Errorf(msg)
	}
	return err
}

func (v Version) MinPatch(major, minor, patch int) (err error) {
	maj := v.MinMajor(major)
	if maj != nil {
		return maj
	}
	min := v.MinMinor(major, minor)
	if min != nil {
		return min
	}
	if v.Major == major && v.Minor == minor && v.Patch < patch {
		msg := fmt.Sprintf("%d.%d.%d < %d.%d.%d", v.Major, v.Minor, v.Patch, major, minor, patch)
		err = fmt.Errorf(msg)
	}
	return err
}

func (v Version) MaxPatch(major, minor, patch int) (err error) {
	maj := v.MaxMajor(major)
	if maj != nil {
		return maj
	}
	min := v.MaxMinor(major, minor)
	if min != nil {
		return min
	}
	if v.Major == major && v.Minor == minor && patch > 0 && v.Patch > patch {
		msg := fmt.Sprintf("%d.%d.%d > %d.%d.%d", v.Major, v.Minor, v.Patch, major, minor, patch)
		err = fmt.Errorf(msg)
	}
	return err
}
