package cmd

import (
	"embed"
	"fmt"
	"log"
	"path/filepath"

	"github.com/urfave/cli/v2"
	qfiles "gitlab.com/gnib-golang/qshuf-check/pkg/files"
	"gitlab.com/gnib-golang/qshuf-check/pkg/requirements"
	qyaml "gitlab.com/gnib-golang/qshuf-check/pkg/yaml"
)

func Validate(c *cli.Context) (err error) {
	if c.Bool("show-workshops") {
		fmt.Println("Available workshops:")
		workshops, e := qfiles.ListWorkshops()
		if e != nil {
			log.Fatal(e)
		}
		for _, ws := range workshops {
			fmt.Printf(" - %s\n", ws)
		}
		return
	}
	yReq, err := qyaml.ParseRequirements(c)
	req, err := requirements.NewRequirements(yReq)
	req.Eval()
	return err
}

func getAllFilenames(fs *embed.FS, path string) (out []string, err error) {
	if len(path) == 0 {
		path = "."
	}
	entries, err := fs.ReadDir(path)
	if err != nil {
		return nil, err
	}
	for _, entry := range entries {
		fp := filepath.Join(path, entry.Name())
		if entry.IsDir() {
			res, err := getAllFilenames(fs, fp)
			if err != nil {
				return nil, err
			}
			out = append(out, res...)
			continue
		}
		out = append(out, fp)
	}
	return
}
