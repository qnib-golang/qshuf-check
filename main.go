package main

import (
	"fmt"
	"log"
	"os"

	"github.com/urfave/cli/v2"
	"gitlab.com/gnib-golang/qshuf-check/pkg/cmd"
)

func main() {
	app := &cli.App{}
	app.EnableBashCompletion = false
	app.Action = func(c *cli.Context) error {
		if c.Bool("version") {
			fmt.Println(`v0.1.9`)
		}
		return nil
	}
	app.Flags = []cli.Flag{
		&cli.BoolFlag{
			Name:    "version",
			Aliases: []string{"V"},
			Usage:   "print version and exit",
		},
	}
	app.Commands = []*cli.Command{
		// validate consumes a workshop name and checks wether the requrirements are met
		/// software version X + device Y + target Z
		{
			Name:   "validate",
			Usage:  "Consumes workshop name and checks wether the requrirements are met",
			Action: cmd.Validate,
			Flags: []cli.Flag{
				&cli.StringFlag{
					Name:    "requirements",
					Aliases: []string{"r"},
					Usage:   "The requirements file to use",
				}, &cli.StringFlag{
					Name:    "workshop",
					Aliases: []string{"ws"},
					Usage:   "Workshop name (reading from embedded files)",
					EnvVars: []string{"QSHUF_WORKSHOP"},
				}, &cli.BoolFlag{
					Name:  "show-workshops",
					Usage: "Show available workshops",
				},
			},
		},
	}
	err := app.Run(os.Args)
	if err != nil {
		log.Fatal(err)
	}
}
